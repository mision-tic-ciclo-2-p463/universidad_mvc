package com.universidad.vista;

import javax.swing.JOptionPane;

import com.universidad.controlador.ControladorUniversidad;
import com.universidad.modelo.Universidad;

public class Registro {

    // Atributos
    ControladorUniversidad cUniversidad;

    // constructor
    public Registro() {
        this.cUniversidad = new ControladorUniversidad();
        // this.solicitar_datos();
        this.menu();
    }

    // Metodos/acciones
    public void solicitar_datos() {
        // Solicitar los datos
        String nombre = JOptionPane.showInputDialog(null, "Por favor ingrese el nombre de la universidad");
        String direccion = JOptionPane.showInputDialog(null,
                "Por favor ingrese la dirección de la universidad " + nombre);
        String nit = JOptionPane.showInputDialog(null, "Por favor ingrese el nit de la universidad " + nombre);
        // Creamos la universidad
        this.cUniversidad.crearUniversidad(nombre, direccion, nit);
        // Mensaje informativo
        JOptionPane.showMessageDialog(null, "¡Universidad creada con éxito!");
        this.menu();

    }

    public void menu() {
        String info = "1 -> Crear universidad\n";
        info += "2 -> Ver universidades\n";
        info += "0 -> Salir\n";

        int opcion = Integer.parseInt(JOptionPane.showInputDialog(null, info));
        switch (opcion) {
            case 1:
                this.solicitar_datos();
                break;
            case 2:
                // ver universidades
                this.consultar_datos();
                break;
            case 0:
                System.exit(0);
                break;
        }

    }

    public void consultar_datos() {
        String infoUniversidad = "";
        // String direccion;
        // String nit;

        for (Universidad Universidad : this.cUniversidad.getArrayUniversidad()) {

            infoUniversidad += "\nNombre universidad: " + Universidad.getNombre();
            infoUniversidad += "\nDirección universidad: " + Universidad.getDireccion();
            infoUniversidad += "\nNit universidad: " + Universidad.getNit();
            infoUniversidad += "\n--------------------------------------------- ";

        }

        JOptionPane.showMessageDialog(null, infoUniversidad);
        this.menu();

    }

}